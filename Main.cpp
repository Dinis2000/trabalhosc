#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
using namespace std;

class termometro {
	const int escala = 2;
	int temperatura;
public:
	//Bob
	termometro(int temp) {
		if (temp % escala == 0)
			temperatura = temp;
		else
			temperatura = (temp + 1);
	}
	//aumenta temp
	int aumenta(void) {
		temperatura += escala;
		return temperatura;
	}
	//diminui temp
	int diminui(void) {
		temperatura -= escala;
		return temperatura;
	}
	//mostra temp
	void mostrar(void) {
		cout << "Temperatura atual: " << temperatura << endl;
	}
};

int main(){
	int temperatura;
	char ch = ' ';
	cout << "Introduza a temperatura atual: "; cin >> temperatura;
	termometro tem(temperatura);
	while (ch != 'X' && ch != 'x')
	{
		system("CLS");
		cout << "--Menu--\nAumentar a temperatura(a)\nDiminuir a temperatura(d)\nVer a temperatura atual(v)\nExit(x)\n>"; cin >> ch; getchar();
		switch (ch)
		{
		case 'A':
		case 'a':
			temperatura = tem.aumenta();
			cout << "temperatura alterada para: " << temperatura << endl;
			break;
		case 'D':
		case 'd':
			temperatura = tem.diminui();
			cout << "temperatura alterada para: " << temperatura << endl;
			break;
		case 'V':
		case 'v':
			tem.mostrar();
			break;
		case 'X':
		case 'x':
			break;
		default:
			cout << "Opcao invalida" << endl;
			break;
		}
		system("pause");
	}
	return 0;
}